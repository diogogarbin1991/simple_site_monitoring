# Simple_Site_Monitoring

Bash script that generates a http code file to monitoring sites links

# Installation

Create a folder into /opt named sites, inside this folder create a text file called sites.txt with the URL's that you want to monitoring, put the _check_sites.sh on the same folder

# How to use 

just run the _check_sites.sh (/opt/sites/_check_sites.sh), the script will create a text file into this folder with a http code returned by the website, the text file generated name will have | to substitute the / on the name, cause you can't create any file with / in the name, so if you open this file on terminal, remember to put \ before all | 

# How it works

The scipt works with double-check to test the websites, Curl and wget, if the website is OK the text file generated will contents 0, although if it fails the text file will have a http code error. Then you can put it on a scheduled cron task to run any time you want with the often thst you need. I used this on Zabbix and works very well