#!/bin/bash
#Autor: Diogo Garbin
#email:diogo.garbin@yahoo.com.br


for x in `cat /opt/sites.txt`; do
touch /opt/sites/$(echo "$x" | sed 's;/;|;g')

    step_one=$(curl -s -o /dev/null -w "%{http_code}" $x | grep 200 )
    if [ -n "$step_one" ]; then
        step_two=$(wget --server-response $x 2>&1 | awk '/^  HTTP/{print $2}' | grep 200)
               if [ -n "$step_two" ]; then
                 echo 0 > /opt/sites/$(echo "$x" | sed 's;/;|;g')
               else 
                 echo "$step_two" > /opt/sites/$(echo "$x" | sed 's;/;|;g')
               fi
      else
          echo "$step_one" > /opt/sites/$(echo "$x" | sed 's;/;|;g')
    fi
done
